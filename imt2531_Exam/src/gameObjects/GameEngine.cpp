#include <GameEngine.h>





//Initialize() Initializes everything that needs to be initialized:
void GameEngine::initializeEverything()
{
	initiateHardcodedMeshes();
	// 0 = box // 1 = axe // 2 = deer

    //Player object
	initializeGameObject(&this->meshHandles[0], Transform{ playerSpawnPos, glm::vec3(0.3f, 0.03f, 0.3f), glm::vec3(0.f, 1.f, 0.f), 0, glm::vec3(1.f, 1.f, 1.f) }, &world);
	objects[0]->setRigidbody(Square::RIGITYPE::Player);
	objects[0]->setObjType(GameObject::OBJTYPE::PLAYER);
	playerObj = objects[0];
	// Player collision checker
	initializeGameObject(&this->meshHandles[0], Transform{ glm::vec3(4.0f, 3.f, 10.f), glm::vec3(0.1f, 0.01f, 0.1f), glm::vec3(0.f, 1.f, 0.f), 0, glm::vec3(1.f, 1.f, 1.f) }, &world);
	objects[1]->setRigidbody(Square::RIGITYPE::Ray);
	objects[1]->isMoving = true;

	player.initialize(&gameCamera,
		*&objects[0],
		*&objects[1],
		&world
	);
	// terrain is a box. can load quad terrain but that is without collision
	initializeGameObject(&this->meshHandles[0], Transform{ glm::vec3(10.0f, 0.5f, 1.8f), glm::vec3(mapSizeX, mapGroundHeight, mapSizeZ), glm::vec3(0.f, 1.f, 0.f), 0, glm::vec3(1.f, 1.f, 1.f) }, &world);
	objects[2]->setObjType(GameObject::OBJTYPE::NONE);
	


	heightMapGenerator("resources/levels/Gjovik_Height MapLow.png");
    
}
void GameEngine::heightMapGenerator(const char* file_name) {
	int height;
	int width;
	int comps = 4;       
	int channelCount = 0;
	GLfloat max = 127;


	std::vector<float> map;
	unsigned int type;
	// Get height map
	unsigned char* image = stbi_load(file_name, &height, &width, &comps, channelCount);
	// size of HeightMap
	height = 60;
	width  = 40;
	// where on height map to read from
	int offsetZ = 0;
	int offsetX = 0;
	
	// min and max values of heightMap
	float maxHeight = 0;
	float minHeight = 0;

	
	// calculate heightMap Y axis
	for (size_t z = offsetZ; z < height + offsetZ; z++)
	{
		for (size_t x = offsetX; x < width + offsetX;  x++)
		{
			//unsigned char r[0], g[1], b[2];
			// Get pixel
			unsigned char* pixelOffset = image + (z * x + x);
			// divide on max pixel value
			float tAxis = ((float)pixelOffset[0] / 256.0);
			// add to height map
			map.push_back(tAxis);
		}
	}
	// Get minimum and maximum value of heightMap
	maxHeight = *std::max_element(map.begin(), map.end());
	minHeight = *std::min_element(map.begin(), map.end());

	// free image
	stbi_image_free(image);

	// Index for new objects 
	int index = objects.size();

	for (size_t z = 0; z < height; z++)
	{
		for (size_t x = 0; x < width; x++)
		{
			// Normalize height map
			float heightMap = (map[z * width + x] - minHeight) / (maxHeight - minHeight) * normalizeMax;
			map[z * width + x] = heightMap;
			// spawn cube
			initializeGameObject(&this->meshHandles[5],
				Transform{ glm::vec3(x * cubeSpace, heightMap + 2, z * cubeSpace), glm::vec3(cubeSize, cubeHeight, cubeSize),
				glm::vec3(0.f, 1.f, 0.f), 0, glm::vec3(0.f, 0.f, 0.f) }, &world);
			// Check if below water level
			if (heightMap < waterHeight)
			{
				// Set Mesh ID and texture ID // Grass
				objects[index]->setMeshID(&this->meshHandles[5]);
			}
			else
			{
				// Water
				objects[index]->setMeshID(&this->meshHandles[0]);
			}
			index++;
		}
	}
	for (size_t z = 0; z < height; z++)
	{
		for (size_t x = 0; x < width; x++)
		{
			// Random int for spawning objects on map
			int objSpawn = rand() % 16;
			if (objSpawn == 1)
			{
				// Deer object
				initializeGameObject(&this->meshHandles[3],
					Transform{ glm::vec3(x * cubeSpace, map[z * width + x] + deerHeight, z * cubeSpace),
				glm::vec3(deerSize), glm::vec3(0.f, 1.f, 0.f), 0, glm::vec3(0.f, 0.f, 0.f) }, &world);
				// Set object type
				objects[index]->setObjType(GameObject::OBJTYPE::DEER);
			}
			else if (objSpawn == 2 || objSpawn == 3)
			{
				// Tree object
				initializeGameObject(&this->meshHandles[4], Transform{ glm::vec3(x * cubeSpace, map[z * width + x] + deerHeight, z * cubeSpace),
				glm::vec3(treeSize), glm::vec3(0.f, 1.f, 0.f), 0, glm::vec3(0.f, 0.f, 0.f) }, &world);
				objects[index]->setObjType(GameObject::OBJTYPE::TREE);
			}
		}
	}
}

void GameEngine::initializeGameObject(int *ID, Transform tr, GameObject* par)
{
	element = new GameObject(ID, tr, par);
	
	objects.push_back(element);
}

void GameEngine::initiateHardcodedDummyList()
{
	DUMMYamountOfMeshes = 7; // 3 // add
	
	// last one unique mesh // unique = own function for generating mesh
	DUMMYmodelFileNames = { "untitled.obj", "axe.obj", "deer.obj", "scrubPine.obj", "untitled.obj", "newUnique" };

	//for each int vector it tells which meshes should be assinged this texture:
	//first textures shall be shader by mesh 0 and 1, 
	DUMMYtexturesFileNames = { "grass.png", "axe_color.png", "deerbody.png", "Tree.png", "water.png", "diamond.png", };
	
	
	DUMMYvertexShaderFileNames = { "sprite_vertex.vert" };
	DUMMYfragmentShaderFileNames = { "sprite_fragment.frag" };

	
	//while the third mesh will have the last texture alone:
	DUMMYmeshesToAssignTexture = { {0,1},{2}, {3}, {4}, {5}, {6} };
	//first model shall be shader by mesh 0 and 1, 
	//while the third mesh will have the last model alone:
	DUMMYmeshesToAssignModels = { {0,1},{2}, {3}, {4}, {5}, {6} };
	//all the meshes will be sharing the same shader program:
	DUMMYmeshesToAssignShaders = { {0, 1, 2, 3, 4, 5, 6} };	
	DUMMYgameObjectToAssignMeshes = { {0, 1, 2}, {} , {} };
}

void GameEngine::initiateHardcodedMeshes()
{
	this->initiateHardcodedDummyList();

	for (int h = 0; h < DUMMYamountOfMeshes; h++)
	{
		mesh.addMesh(&this->meshHandles[h]);
	}

	
	//Assigning meshes a model/renderables:
	for (int i = 0; i < DUMMYmeshesToAssignModels.size(); i++)
	{
		
		//Reserving a pointer in mesh to then be give to the renderer:
		auto ID = this->mesh.allocateModelID();
		//Loading model:
		if (i < DUMMYmeshesToAssignModels.size() -1)
		{
			gameRenderer.addRenderable(ID, ("resources/objects/" + DUMMYmodelFileNames[i]).c_str());
		}
		else
		{
			// not used. this creates a unique mesh(Quad).
			gameRenderer.addHeightMap(ID, "resources/levels/Gjovik_Height Map.png");
			
		}
		//Looping through all the meshes which is assigned to this model:
		for (int j = 0; j < DUMMYmeshesToAssignModels[i].size(); j++)
		{
			//Assigns this meshes ID pointer to the ID recieved from the renderer:
			mesh.meshDatas[DUMMYmeshesToAssignModels[i][j]].virtualModelID = ID;
		}
	}

	//Assigning meshes a texture:
	for (int i = 0; i < DUMMYmeshesToAssignTexture.size(); i++)
	{
		//Reserving a pointer in mesh to then be give to the renderer:
		auto ID = this->mesh.allocateTextureID();
		//Loading model:
		gameRenderer.addTexture(ID, "resources/textures/" + DUMMYtexturesFileNames[i], GL_REPEAT, GL_NEAREST_MIPMAP_NEAREST, GL_NEAREST);
		//Looping through all the meshes which is assigned to this model:
		for (int j = 0; j < DUMMYmeshesToAssignTexture[i].size(); j++)
		{
			//Assigns this meshes ID pointer to the ID recieved from the renderer:
			mesh.meshDatas[DUMMYmeshesToAssignTexture[i][j]].virtualTextureID = ID;
		}
	}
	//Assigning meshes a shader:
	for (int i = 0; i < DUMMYmeshesToAssignShaders.size(); i++)
	{
		//Reserving a pointer in mesh to then be give to the renderer:
		auto ID = this->mesh.allocateShaderID();
		//Loading model:
		gameRenderer.addShader(ID, ("resources/shaders/" + DUMMYvertexShaderFileNames[i]).c_str(), ("resources/shaders/" + DUMMYfragmentShaderFileNames[i]).c_str());
		//Looping through all the meshes which is assigned to this model:
		for (int j = 0; j < DUMMYmeshesToAssignShaders[i].size(); j++)
		{
			//Assigns this meshes ID pointer to the ID recieved from the renderer:
			mesh.meshDatas[DUMMYmeshesToAssignShaders[i][j]].virtualShaderID = ID;
		}
	}
	if (DUMMYamountOfMeshes > 0)
	{

		std::cout << "hei2\n"
			<< this->DUMMYmodelFileNames[0].c_str() << std::endl
			<< this->DUMMYtexturesFileNames[0].c_str() << std::endl;
	}

}

void GameEngine::update(GLFWwindow* gameWindow, GLdouble deltaTime)
{
	// process camera input
	this->gameCamera.processInput(gameWindow, deltaTime);
	// set camrea position // Lookat 
	this->gameRenderer.setCamera(this->gameCamera.getCameraPositionVector(), this->gameCamera.getCameraTargetVector(), this->gameCamera.getCameraUpVector());

    //Player
    player.update(gameWindow, deltaTime);

	changeCameraView(gameWindow);
}
// Changes camera to deer model or tree model
void GameEngine::changeCameraView(GLFWwindow* gameWindow) {
	
	if (glfwGetKey(gameWindow, GLFW_KEY_Z) == GLFW_PRESS && changeObjPress == false)
	{
		changeObjPress = true;
		for (size_t i = 0; i < objects.size(); i++)
		{
			//  not same as current type and NONE type
			if (objects[objIndex]->getObjType() != player.getObjType() && objects[objIndex]->getObjType() != GameObject::NONE)
			{
				player.setObj(objects[objIndex], objects[objIndex]->getObjType());
				i = objects.size();
			}
			objIndex++;
			if (objIndex >= objects.size()) {
				objIndex = 0;
				// reset loop
				i = objects.size();
			}
		}
	}
	// Reset change object type
	else if (glfwGetKey(gameWindow, GLFW_KEY_X) == GLFW_PRESS) {
		changeObjPress = false;
	}
	// Set player obj type back to player type
	else if (glfwGetKey(gameWindow, GLFW_KEY_Q) == GLFW_PRESS) {
		playerObj->transform.position = playerSpawnPos;
		player.setObj(playerObj, playerObj->getObjType());
	}
}

void GameEngine::render(GLdouble timeSinceLastFrame, GLdouble time)
{
	lightSpeed += 0.04;
	// adding light to the scene
	gameRenderer.addLight(&lightID, glm::vec3(ligPosS.x + lightSpeed, ligPosS.y, ligPosS.z), glm::vec3(lightStr*4), glm::vec3(0, 0, 0), glm::vec3(0, 0, 0), 1, 1, 1);

	for (GameObject* child : world.children) renderObjects(child, time);
	
	
	// draw scene
	gameRenderer.drawScene(1);
	// remove light
	gameRenderer.removeLight(lightID);
}

void GameEngine::renderObjects(GameObject* obj, GLdouble time)
{

    obj->update(time);
	// Checking collision
	if (obj->HasVelocity() || obj->cur == Square::Ray)
	{

		for (GameObject* child : world.children)
		{
			if (child != obj) obj->IntersectWithObject(child);
		}
	}

	auto meshToRender = mesh.getMesh(obj->getMeshID());

	gameRenderer.addToScene(*meshToRender->virtualModelID, *meshToRender->virtualTextureID, *meshToRender->virtualShaderID, &obj->transform.position,
		&obj->transform.scale, &obj->transform.rotate, &obj->transform.rotationAxis, &obj->transform.color);

	// Run children renderObject for this gameobject
    for (GameObject* child : obj->children) renderObjects(child, time);
}