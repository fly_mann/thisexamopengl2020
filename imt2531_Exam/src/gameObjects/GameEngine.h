#ifndef GAME_OBJECTS_H
#define GAME_OBJECTS_H
//#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <GLM/trigonometric.hpp>
#include <renderer_model.h>
#include <renderer.h>
#include <renderer_sprite.h>
#include <renderer_vertex_data.h>
#include <camera.h>
#include <game_object.h>		// has vector
#include <global_game_variables.h>

#include <game_movement.h>
#include <game_platform.h>
#include <game_rigidbody.h>
#include <game_mesh.h>		
//#include <game_physics_engine.h>


//ALL GameObjects use the same Renderable because
//they are all (Wether they are the player, ghost, balls)
//rendered as a sprite with the same Vertex_structure and attributes.
//Therefore they will use the same VBO and VAO (and EBO):
class GameEngine
{
private:
	
	int objIndex = 0;
	// for big quad object pos. Ground below heightMap
	const int mapSizeX = 500;
	const int mapSizeZ = 500;
	const int mapGroundHeight = 1.f;
	// spaceBetween objects
	const int space = 20;
	// Reset changing object type
	bool changeObjPress = false;
	// where player spawn
	const glm::vec3 playerSpawnPos = glm::vec3(10.5f, 80, 10.5f);
	// scale heightMap up
	const float normalizeMax = 3;
	// cube size for heightMap
	const float cubeSize = 1;
	float cubeSpace = cubeSize * 2;
	float cubeHeight = 5;
	// if below water height change texture
	float waterHeight = 1.5;
	float deerHeight = 7;
	// size of objects
	float deerSize = 0.2f;
	float treeSize = 0.006;

	int lightID = 0;
	// light move speed
	float lightStr = 0.1f;
	float lightSpeed = 0.5f;
	// light start pos and end pos
	glm::vec3 ligPosS = glm::vec3(playerSpawnPos.x - 1, playerSpawnPos.y, playerSpawnPos.z);
	glm::vec3 ligPosE = glm::vec3(playerSpawnPos.x + 100, playerSpawnPos.y, playerSpawnPos.z);
	
	// store mesh, texture etc Index
	std::vector<std::vector<int>> DUMMYmeshesToAssignTexture;
	std::vector<std::vector<int>> DUMMYmeshesToAssignModels;
	std::vector<std::vector<int>> DUMMYmeshesToAssignShaders;
	std::vector<std::vector<int>> DUMMYgameObjectToAssignMeshes;
	std::vector<std::string> DUMMYtexturesFileNames;
	std::vector<std::string> DUMMYmodelFileNames;
	std::vector<std::string> DUMMYvertexShaderFileNames;
	std::vector<std::string> DUMMYfragmentShaderFileNames;
	int DUMMYamountOfMeshes;

	void initiateHardcodedDummyList();

	glm::vec2 size = { 0.f, 0.f };
	GLuint width{}, height{};
	Renderer gameRenderer = Renderer(GAME_WINDOW_WIDTH, GAME_WINDOW_HEIGHT);
	Camera gameCamera;
#pragma region GameScript
	Movement player;// = Movement();
#pragma endregion


	Mesh mesh = Mesh();

	std::vector<int> meshHandles = std::vector<int>(MAX_TOTAL_MESHES);

	GameObject world = GameObject();					// Worldview
	GameObject* element;				// Individual element being created
	GameObject* playerObj;				// Individual element being created
	std::vector<GameObject*> objects;	// All elements created are being stored in an array

	//PhysicsEngine physicsEngine = PhysicsEngine();
	void renderObjects(GameObject* obj, GLdouble time);

public:

	GameEngine() { }
	~GameEngine() {
		
		for (auto i : objects) {
			delete i;
		}
		delete element;
	}
	void initializeEverything();
	void heightMapGenerator(const char* file_name);
	void initializeGameObject(int* ID, Transform tr, GameObject* par);
	void initiateHardcodedMeshes();
	void update(GLFWwindow* gameWindow, GLdouble deltaTime);
	void render(GLdouble timeSinceLastFrame, GLdouble time);
	void updateInputScrollPosition();
	void updateInputCursorPosition();
	void updateInputKey(GLFWwindow* gameWindow, GLdouble& deltaTime, GLdouble& time);

	void addLight(int x);
	void changeCameraView(GLFWwindow* gameWindow);

};
#endif //!GAMEOBJECTS_H
