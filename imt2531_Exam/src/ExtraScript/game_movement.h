#ifndef GAME_MOVEMENT_H
#define GAME_MOVEMENT_H

#include <camera.h>
#include <game_object.h>
#include <global_game_variables.h>
#include <glm/glm.hpp>
#include <glad/glad.h>
#include <GLFW/glfw3.h>


class Movement {
public:
    Movement();
    ~Movement() {
        delete camera;
    }
    void initialize(Camera *cam, GameObject *obj, GameObject* objGround, GameObject *world);

    void processInput(GLFWwindow* window, GLdouble deltaTime);
    void update(GLFWwindow* window, GLdouble deltaTime);
    void jumping(GLFWwindow* window, GLdouble deltaTime);
    void movement(GLFWwindow* window, GLdouble deltatime);

    void setObj(GameObject* obj) { this->obj = obj; }
    
    void setObj(GameObject* newObj, GameObject::OBJTYPE newType) { this->obj = newObj; this->curType = newType; };
    GameObject::OBJTYPE getObjType() { return this->curType; };
private:
    GameObject::OBJTYPE curType;
#pragma region cameraObjHeight
    int deerHeight = 3;
    int treeHeight = 6;
#pragma endregion

    
    Camera *camera;
    GameObject* obj;
    // ground checker
    GameObject* objGround;
    GameObject *world;
    // floor pos.y
    float floorPos;
    GLboolean canJump = false;
    GLboolean jump = false;
    GLboolean atPeak = true; /// true
    GLboolean landed = false;
    // player movement speed
    float moveSpeed = 1.5f;
    float headPosForCamera = 3.0f;
    float groundCheckerPos = 0.1f;
    float heightSpeed = 0.1f;
    // peak of jump
    float peak = 1.f;
    // Gravity and jump force
    float force = 0;
    // Gravity and jump force
    glm::vec3 forceVec;
    // jump force
    float addForce = 0.9;
    float removeForce = 0.0004f;
    GameInput& globalGameInput = GameInput::GetInstance();

    glm::vec3 gameCameraTarget{};
};

#endif //!GAMEMOVEMENT_H