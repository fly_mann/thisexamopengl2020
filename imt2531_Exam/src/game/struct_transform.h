#ifndef STRUCT_TRANSFORM_H
#define STRUCT_TRANSFORM_H

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <string>
#include <vector>

struct Transform
{
public:
//	Transform (glm::vec3 _pos, glm::vec3 _scale, glm::vec3 _rotation, glm::vec3 _velocity) :
//		position(_pos), scale(_scale), rotation(_rotation), m_velocity(_velocity) {}
	glm::vec3 position;
	glm::vec3 scale;
	glm::vec3 rotationAxis;
	float rotate;
	glm::vec3 color;

	// speed, direction
	glm::vec3 m_velocity;
	
};


#endif //!STRUCT_TRANSFORM_H