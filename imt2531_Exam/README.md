Requirements
	cmake_minimum_required 3.14
	Version 2.0.0
	C++ 17 Compliant Compiler
	GLFW, GLM 

Performance
	In folder gamObjects, GameEngine.cpp, func heightMapGenerator there are 2 values height and width.
	Lower the values for better performance. Height and width = size of map.

Controls
	Movement: WASD.
	Mouse Movement.
	Zoom in and out with mouse scroll.
	SHIFT key for going up, CTRL for going down.
	SPACE for jumping.
	Change between object using X key and then Z key.
	Q key for going back to player at starting position.
	J,K,K,L for turning on lights.
	Y,U,I,O for turning off lights.
	
Game
	When the game starts it will load a height map made out of cubes.
	It will spawn cubes with water texture if below water level,
	and other objects like deer and trees.

Notes
	Can't change to deer object.	
	The deer and tree does not rotate when controlling it. 
	The game does contains collision detection, but player will go down in Y axis a little bit when standing on the ground.
	

Demo video

